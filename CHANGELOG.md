

## [0.2.0](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/compare/0.2.0..0.1.0) (2023-09-10)


### Features

* FME-11 Env Configuration Service ([4b4a771](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/4b4a77120779b89125b0caa9b25935157788ab5f))
* FME-5 Swagger Integration ([b872413](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/b872413b5404b7b9068e00c5d95daec8194808d2))


### Bug Fixes

* FME-12 Fix Testing suite ([246ed1a](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/246ed1a28548c6372b8a3e361c7b7b23c9f60912))

## [0.1.0](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/compare/0.1.0..0.0.3) (2023-09-09)


### Features

* FME-2 add git pre-push hook ([e192988](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/e192988379e91ade7ab0aa2039ff5352ea6d2916))
* FME-4 add docker support for local dev ([07a5e97](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/07a5e9745ed36f509c6f3e6c432af8790142ae95))
* FME-4 add magic link login schema ([19b5238](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/19b5238c329a8e5b0bb652e41163864bf4a610f6))
* FME-4 add migration file and tenancy seed ([b69aaad](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/b69aaad311f8296cd6704dc9a4cfc0c15b6637ea))
* FME-4 add prisma configurations ([499d4e0](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/499d4e0d1be1d94cb34d94b936cde49b6f7bd128))
* FME-4 add tenancy prisma schemas ([37fdd91](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/37fdd91e95d62ce9ae6ab1001f996ce5438a8224))


### Bug Fixes

* FME-4 get ticket number from branch ([ab47dd8](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/ab47dd85b329760a5bb067ec4d4278d224ba5f8b))
* FME-4 tenancy relationship with roles and users ([a88025e](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/commit/a88025e56518dc1943c7fd03dae51ab99aa5a6f2))

## [0.0.3](https://gitlab.com/fair-mate-expenses/backend/apis/monolithic/implementation/compare/0.0.3..0.0.2) (2023-09-07)

## 0.0.2 (2023-09-07)