import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { addSwaggerDocument } from 'src/utils/swagger/swagger.utils';
import { EnvironmentConfigServiceService } from 'src/common/config/services/environment-config-service/environment-config-service.service';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(EnvironmentConfigServiceService);
  configService.validateEnvVariables();

  addSwaggerDocument(app);

  const port = configService.getPort();
  const logger = new Logger('Bootstrap');
  logger.log(`Server: http://localhost:${port}`);
  logger.log(`Environment: ${process.env.NODE_ENV}`);
  await app.listen(port);
}
bootstrap();
