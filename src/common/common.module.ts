import { Module } from '@nestjs/common';
import { EnvironmentConfigServiceService } from './config/services/environment-config-service/environment-config-service.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: process.env.NODE_ENV ? '.env.production' : '.env.local',
    }),
  ],
  providers: [EnvironmentConfigServiceService],
  exports: [EnvironmentConfigServiceService],
})
export class CommonModule {}
