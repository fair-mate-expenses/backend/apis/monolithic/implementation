import { Test, TestingModule } from '@nestjs/testing';
import { EnvironmentConfigServiceService } from './environment-config-service.service';
import { ConfigService } from '@nestjs/config';

describe('EnvironmentConfigServiceService', () => {
  let service: EnvironmentConfigServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EnvironmentConfigServiceService, ConfigService],
    }).compile();

    service = module.get<EnvironmentConfigServiceService>(
      EnvironmentConfigServiceService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
