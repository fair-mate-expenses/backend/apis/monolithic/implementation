import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  AVAILABLE_ENV_VARS,
  MANDATORY_ENV_VARS,
  SwaggerServer,
} from './environment-config.models';
import { iterateEnumStringValues } from 'src/utils/enum.utils';

@Injectable()
export class EnvironmentConfigServiceService {
  constructor(private configService: ConfigService) {}

  getPort(): number {
    return this.configService.get<number>(
      AVAILABLE_ENV_VARS.APP_PORT.toString(),
      4000,
    );
  }

  getServers(): SwaggerServer[] {
    return (
      this.configService.get<SwaggerServer[]>(
        AVAILABLE_ENV_VARS.SERVER_LIST.toString(),
      ) || []
    );
  }

  validateEnvVariables(): void {
    // Check if required environment variables are provided
    iterateEnumStringValues(MANDATORY_ENV_VARS).forEach((v) => {
      if (!process.env[v]) {
        Logger.error(
          `Required environment variable (${v}) is missing. Exiting...`,
        );
        process.exit(1); // Exit the application with an error code
      }
    });
  }
}
