import { combineEnums } from 'src/utils/enum.utils';

export enum MANDATORY_ENV_VARS {
  APP_PORT = 'APP_PORT',
}

enum OPTIONAL_ENV_VARS {
  SERVER_LIST = 'SERVER_LIST',
}

export const AVAILABLE_ENV_VARS = combineEnums(
  MANDATORY_ENV_VARS,
  OPTIONAL_ENV_VARS,
);

export type SwaggerServer = {
  url: string;
  description?: string;
};
