export const iterateEnumStringValues = (enumObject: any): string[] => {
  const stringValues: string[] = [];

  for (const key in enumObject) {
    const value = enumObject[key];
    if (typeof value === 'string') {
      stringValues.push(value);
    }
  }
  return stringValues;
};

// Utility function to combine enums without duplicating values
export function combineEnums(
  ...enums: Record<string, string>[]
): Record<string, string> {
  return Object.assign({}, ...enums);
}
