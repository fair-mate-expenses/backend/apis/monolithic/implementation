import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as packageJson from 'src/../package.json';
import { EnvironmentConfigServiceService } from 'src/common/config/services/environment-config-service/environment-config-service.service';

const version = packageJson.version;

export const addSwaggerDocument = (app: INestApplication<any>) => {
  const configService = app.get(EnvironmentConfigServiceService);

  const config = buildConfig(configService);

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('', app, document);
};

const buildConfig = (configService: EnvironmentConfigServiceService) => {
  const config = new DocumentBuilder()
    .setTitle('Fair Mate Expenses - API - Monolithic')
    .setVersion(version);

  //Add servers from env file
  configService
    .getServers()
    .forEach((s) => config.addServer(s.url, s.description));

  // Add only localhost server when developing locally
  if (process.env.NODE_ENV === 'development') {
    config.addServer(
      `http://localhost:${configService.getPort()}`,
      'local server',
    );
  }

  return config.build();
};
