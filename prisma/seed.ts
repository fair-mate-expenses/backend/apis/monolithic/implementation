// seed.ts

import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function seed() {
  try {
    // Create roles
    const adminRole = await prisma.role.create({
      data: {
        name: 'admin',
      },
    });

    const ownerRole = await prisma.role.create({
      data: {
        name: 'owner',
      },
    });

    const managerRole = await prisma.role.create({
      data: {
        name: 'manager',
      },
    });

    const viewerRole = await prisma.role.create({
      data: {
        name: 'viewer',
      },
    });

    // Create admin user
    await prisma.user.create({
      data: {
        username: 'admin',
        email: 'admin@example.com',
        phone: '1234567890', // Replace with an actual phone number
        firstName: 'Admin',
        lastName: 'User',
        roles: {
          connect: {
            id: adminRole.id,
          },
        },
      },
    });

    // Create accounts and connect users to them with roles
    for (let i = 1; i <= 5; i++) {
      const userOwner = await prisma.user.create({
        data: {
          username: `owner${i}`,
          email: `owner${i}@example.com`,
          phone: `111111111${i}`, // Replace with an actual phone number
          firstName: `Owner ${i}`,
          lastName: 'User',
          magicLinks: {
            create: [
              {
                token: `token-owner-${i}`,
              },
              {
                token: `token-owner-invalid-${i}`,
                isValid: false,
              },
              {
                token: `token-owner-expired-${i}`,
                expiresAt: new Date(Date.now() - 3600000),
              },
            ],
          },
        },
      });

      const userManager = await prisma.user.create({
        data: {
          username: `manager${i}`,
          email: `manager${i}@example.com`,
          phone: `2222222222${i}`, // Replace with an actual phone number
          firstName: `Manager ${i}`,
          lastName: 'User',
          magicLinks: {
            create: [
              {
                token: `token-manager-${i}`,
              },
              {
                token: `token-manager-invalid-${i}`,
                isValid: false,
              },
              {
                token: `token-manager-expired-${i}`,
                expiresAt: new Date(Date.now() - 3600000),
              },
            ],
          },
        },
      });

      const userViewer = await prisma.user.create({
        data: {
          username: `viewer${i}`,
          email: `viewer${i}@example.com`,
          phone: `3333333333${i}`, // Replace with an actual phone number
          firstName: `Viewer ${i}`,
          lastName: 'User',
          magicLinks: {
            create: [
              {
                token: `token-viewer-${i}`,
              },
              {
                token: `token-viewer-invalid-${i}`,
                isValid: false,
              },
              {
                token: `token-viewer-expired-${i}`,
                expiresAt: new Date(Date.now() - 3600000),
              },
            ],
          },
        },
      });

      await prisma.account.create({
        data: {
          name: `Account ${i}`,
          users: {
            create: [
              {
                roleId: ownerRole.id,
                userId: userOwner.id,
              },
              {
                roleId: managerRole.id,
                userId: userManager.id,
              },
              {
                roleId: viewerRole.id,
                userId: userViewer.id,
              },
            ],
          },
        },
      });
    }

    console.log('Seed data created successfully!');
  } catch (error) {
    console.error('Error seeding data:', error);
  } finally {
    await prisma.$disconnect();
  }
}

seed();
