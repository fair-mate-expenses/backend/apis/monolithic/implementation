// schema.prisma

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL") // Automatically uses the URL from the DATABASE_URL environment variable
}

// Define the Account model with UUID primary key
model Account {
  id    String            @id @default(uuid()) // Use UUID as the primary key
  name  String
  users UserAccountRole[] // Define a many-to-many relationship with users

  // Add an index on the 'name' field for faster lookups
  @@index([name])
}

// Define the User model with UUID primary key
model User {
  id        String            @id @default(uuid()) // Use UUID as the primary key
  username  String            @unique
  email     String            @unique
  phone     String            @unique
  firstName String
  lastName  String
  accounts  UserAccountRole[] // Define a many-to-many relationship with accounts and roles
  roles     Role[] // Define a many-to-many relationship with roles
  magicLinks MagicLink[]

  // Add an index on the 'email' field for faster email-based lookups
  @@index([email])
  // Add an index on the 'username' field for faster email-based lookups
  @@index([username])
  // Add an index on the 'phone' field for faster email-based lookups
  @@index([phone])
}

// Define the MagicLink model with UUID primary key
model MagicLink {
  id        String    @id @default(uuid()) // Use UUID as the primary key
  token     String    @unique // Unique token for the magic link
  userId    String // Use UUID as the foreign key to associate with a user
  user      User      @relation(fields: [userId], references: [id])
  createdAt DateTime  @default(now()) // Timestamp when the magic link is created
  expiresAt DateTime? // Timestamp when the magic link expires (optional)
  isValid   Boolean   @default(true)
  @@index([userId])
}

// Define the UserOnAccount join table with UUID primary key
model UserAccountRole {
  id        String  @id @default(uuid()) // Use UUID as the primary key
  user      User    @relation(fields: [userId], references: [id])
  userId    String // Use UUID as the foreign key
  account   Account @relation(fields: [accountId], references: [id])
  accountId String // Use UUID as the foreign key
  role      Role    @relation(fields: [roleId], references: [id])
  roleId    String // Use UUID as the foreign key

  // Add an index on the 'userId', 'accountId', and 'roleId' fields for faster lookups
  @@index([userId, accountId, roleId])
}

// Define the Role model with UUID primary key
model Role {
  id              String            @id @default(uuid()) // Use UUID as the primary key
  name            String
  users           User[]
  UserAccountRole UserAccountRole[]

  // Add an index on the 'name' field for faster lookups
  @@index([name])
}
